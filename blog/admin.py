from django.contrib import admin
from .models import Post, Category, Comment, FeedBack


class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(Post, PostAdmin)
admin.site.register(Category, PostAdmin)
admin.site.register(Comment)
admin.site.register(FeedBack)


# Register your models here.
